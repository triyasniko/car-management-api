# Binar: Car Management API

Repository ini ditujukan sebagai submission challenge 6 Fullstack Web App

## Getting Started
Untuk menjalankan project silahkan ikuti step dibawah ini

1. Clone the repo
```sh
git clone https://gitlab.com/triyasniko/car-management-api
```
2. Install packages
```sh
npm install
```
3. Database requirements
```sh
   - setting file database.js
   - sequelize db:create
   - sequelize db:migrate
   - sequelize db:seed:all
```
4. Run Server
```sh
node bin/www
```
## Info

- Swagger Car Management API

```sh
http://localhost:8000/api/v1/docs/
```