const express = require("express");
const controllers = require("../app/controllers");
const authorization = require("../app/middlewares/authorization")

const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("../data/documentation.json");

const apiRouter = express.Router();

/**
 * TODO: Implement your own API
 *       implementations
 */

apiRouter.get("/api/v1/car/", controllers.api.v1.carController.getAll);
apiRouter.get("/api/v1/car/:id", controllers.api.v1.carController.getById);

// Authentication Route
apiRouter.post("/api/v1/register", controllers.api.v1.userController.register);
apiRouter.post("/api/v1/login", controllers.api.v1.userController.login);

// CRUD Route
// apiRouter.post("/api/v1/car/create", authorization.authorize, controllers.api.v1.carController.create);
// apiRouter.post("/api/v1/car/update/:id", authorization.authorize, controllers.api.v1.carController.update);
// apiRouter.get("/api/v1/car/delete/:id", authorization.authorize, controllers.api.v1.carController.delete);

apiRouter.post("/api/v1/car/create", controllers.api.v1.carController.create);
apiRouter.post("/api/v1/car/update/:id", controllers.api.v1.carController.update);
apiRouter.get("/api/v1/car/delete/:id", authorization.authorize, controllers.api.v1.carController.delete);

apiRouter.use("/api-docs", swaggerUi.serve);
apiRouter.get("/api-docs", swaggerUi.setup(swaggerDocument));

apiRouter.use(controllers.api.main.onLost);
apiRouter.use(controllers.api.main.onError);

module.exports = apiRouter;
