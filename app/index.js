/**
 * @file Bootstrap express.js server
 */

const express = require("express");
const morgan = require("morgan");
const router = require("../config/routes");
require('dotenv').config;

const app = express();

/** Install request logger */
app.use(morgan("dev"));

/** Install JSON request parser */
app.use(express.json());
app.use(express.static('assets'));

/** Install Router */
app.use(router);

module.exports = app;
