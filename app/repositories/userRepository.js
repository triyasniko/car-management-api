const { User } = require("../models");

module.exports = {
  register(data) {
    return User.create(data);
  },

  findByEmail(email) {
    return User.findOne({
      where: { email }
    });
  }
};
