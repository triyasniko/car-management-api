const jwt = require("jsonwebtoken");
const userRepository = require("../repositories/userRepository");

module.exports = {
    authorize(req, res, next) {
        try {
            const bearerToken = req.headers.authorization;
            const token = bearerToken.split('Bearer ')[1];
            const payload = jwt.verify(token, process.env.JWT_SECRET || "Secret");

            req.user = userRepository.findByEmail(payload.email);
            next();
        } catch (err) {
            res.status(400).json({
                status: "FAIL",
                message: err.message,
              });
        }
    }
}